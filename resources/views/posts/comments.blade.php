@foreach($comments as $comment)
    <div class="display-comment" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
        <strong>{{ $comment->user->name }}</strong>
        <p>{{ $comment->body }}</p>
        
        <form method="post" action="{{ route('comments.store') }}">
            @csrf
            <div class="form-group">
                <input type="text" name="body" class="form-control" id="body1_{{ $comment->id }}" />
                <div id="error_{{ $comment->id }}"></div>
                <input type="hidden" name="post_id" value="{{ $post_id }}" id="post_id1" />
                <input type="hidden" name="parent_id" value="{{ $comment->id }}" id="parent_id1" />
            </div>
            <div class="form-group">
                <a href="javascript:void(0);" onclick="addReply('{{ $comment->id }}')" class="btn btn-info">Reply</a>
            </div>
        </form>
        @include('posts.comments', ['comments' => $comment->replies])
    </div>
@endforeach
