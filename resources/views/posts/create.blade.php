@extends('layouts.app')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Post</div>
                <div class="card-body">
                    {!! Form::open(['method' => 'POST', 'route' => ['posts.store'], 'files' => true,]) !!}
                        <div class="form-group">
                            @csrf
                            <label class="label">Title: </label>
                            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}

                        </div>
                        <div class="form-group">
                            <label class="label">Post Content: </label>
                            {!! Form::textarea('content', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group">
                            <label class="label">Image: </label>
                            {!! Form::file('image', ['class' => 'form-control', 'required' => 'required', 'accept'=>"image/*"]) !!}

                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" />
                        
                            <a href="{{url('posts')}}"  class="btn btn-info" >Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection