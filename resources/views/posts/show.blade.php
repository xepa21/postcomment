@extends('layouts.app')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">                    
                    <br/>
                    <h2>{{ $post->title }}</h2>
                    <p>
                        {{ $post->content }}
                    </p>
                    <p>
                        <img src="{{ url('public/uploads/'.$post->image) }}" height="50px" width="50px" >
                    </p>
                    <hr />
                    <h4>Comments</h4>
                    
                    <div id="replace_div">
                    @include('posts.comments', ['comments' => $post->comments, 'post_id' => $post->id])
                    </div>
                    <hr />
                    <h4>Add comment</h4>
                    <form method="post" action="{{ route('comments.store'   ) }}" id="xyz">
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control" name="body" id="body"></textarea>
                            <div id="error"></div>

                            <input type="hidden" name="post_id" value="{{ $post->id }}" id="post_id" />
                        </div>
                        <div class="form-group">
                            <input type="button" onclick="addComment();" class="btn btn-success" value="Add Comment" />
                        </div>
                    </form>
   
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript" src="{{url('/public/js/jquery.min.js')}}"></script>
<script>

  $(document).ready(function(){
    
});
  function addComment() {
    var post_id = $("#post_id").val();
    var body = $("#body").val();
    
    console.log(post_id);
    console.log(body);
    if(body == '') {
        $("#error").html('Enter your comment');
        return false;
    }
    $.ajax({
        url : "{{url('/')}}"+ '/ajax_add_comment/'+post_id+'/'+body,
        type: "GET",
        success: function(data)
        {
            $("#xyz").closest('form').find("input[type=text], textarea").val("");            
            $("#replace_div").empty().html(data);            
        }
    });
    }

    function addReply(parent_id) {
    var post_id = $("#post_id1").val();
    var body = $("#body1_"+parent_id).val();
    // alert(parent_id);
    if(parent_id == undefined) {
        parent_id = '';
    }
    console.log(post_id);
    console.log(body);
    console.log(parent_id);
    if(body == '') {
        $("#error_"+parent_id).html('Enter your comment');
        return false;
    }

    $.ajax({
        url : "{{url('/')}}"+ '/ajax_add_comment/'+post_id+'/'+body+'/'+parent_id,
        type: "GET",
        success: function(data)
        {
            $(this).closest('form').find("input[type=text], textarea").val("");            
            $("#replace_div").empty().html(data);
            
        }
    });
    }
</script>