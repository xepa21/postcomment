Steps: 
1. clone project 
2. composer update 
3. rename .env.example to .env 
4. change db details in .env 
5. create database 
6. php artisan migrate 
7. php artisan key:generate

Redirect to your project directory name in browser. There will be one link in center 'POSTS'