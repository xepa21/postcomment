<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\Comment;
use App\Post;

   
class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'body'=>'required',
        ]);
   
        $input = $request->all();
        $input['user_id'] = auth()->user()->id;
    
        Comment::create($input);
   
        return back();
    }

    public function ajax_add_comment($post_id, $comment,$parent_id = NULL) {
        $input['body'] = $comment;
        $input['post_id'] = $post_id;        
        $input['user_id'] = auth()->user()->id;
        $input['parent_id'] = $parent_id;
    
        Comment::create($input);
        $post = Post::find($post_id);
        $comments = $post->comments;
        
        return view('posts.comments', compact('comments', 'post_id'))->render();        
    }
}