<?php
  
namespace App;
  
use Illuminate\Database\Eloquent\Model;
  
class Post extends Model
{
    
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'image'];
    protected $table = "post";

   
    /**
     * The has Many Relationship
     *
     * @var array
     */
    public function comments()
    {
        return $this->hasMany(Comment::class)->whereNull('parent_id');
    }
}